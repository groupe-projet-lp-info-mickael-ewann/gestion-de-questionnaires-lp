class Question {
  constructor(id, questionCategory, questionType, label) {
    this._id = id;
    this._questionCategory = questionCategory;

    this._questionType = questionType.toString();
    this._answers = new Array(0);
    this._label = label;
    this._questionTypeLabel = _allQuestionTypes[this._questionType.toString()];
  }

  get id() {
    return this._id;
  }

  addAnswer(answer) {
    this._answers.push(answer);
    console.log(this._answers);
  }

  removeAllAnswers() {
    this._answers = new Array();
  }

  get answerCount() {
    return this._answers.length;
  }

  get answers() {
    return this._answers;
  }

  logAllAnswers() {
    for (const answer of this._answers) {
      console.log(answer);
    }
  }

  overrideQuestionCategory(questionCategory) {
    this._questionCategory = questionCategory;
  }

  overrideQuestionType(questionType) {
    this._questionType = questionType.toString();
    this._questionTypeLabel = _allQuestionTypes[this._questionType.toString()];
    console.log("quest type" + this._questionTypeLabel);
  }

  overrideLabel(label) {
    this._label = label;
  }

  get label() {
    return this._label;
  }

  get questionCategory() {
    return this._questionCategory;
  }

  get questionCategoryText() {
    return _allQuestionCategories[this._questionCategory];
  }

  get questionType() {
    return this._questionType;
  }

  get typeText() {
    _allQuestionTypes[this._questionType];
  }

  get needToDisplayMessageAskingForMoreAnswers() {
    if (this._questionType == 1) {
      if (this._answers.length < 2) {
        return true;
      }
    }

    if (this._questionType == 2) {
      if (this._answers.length < 2) {
        console.log("return true 1");

        return true;
      }
    }
    console.log("Asking for more answers: false");
    return false;
  }

  get canBeSubmited() {
    let goodAnswersCount = 0;
    let badAnswersCount = 0;

    this.answers.forEach((answer) => {
      answer.isCorrect ? goodAnswersCount++ : badAnswersCount++;
    });

    switch (this.questionType) {
      case "1":
        if (goodAnswersCount > 1 || this.answerCount < 1) {
            console.log("test a " +  (goodAnswersCount > 1))
            console.log("test b " +  (this.answerCount < 1))
          return false;
        }
        break;
      case "2":
        if (this.answerCount < 2 || this.answerCount > 5) {
            console.log("CASE 2")
          return false;
        }
        break;

      case "3":
        if (
          goodAnswersCount != 1 ||
          badAnswersCount != 1 ||
          this.answerCount != 2
        ) {            console.log("CASE 3");

          return false;
        }
        break;

      case "4":
        if (this.answerCount < 1) {            console.log("CASE 4");

          return false;
        }
        break;

      case "5":            console.log("CASE 5");

        break;
    }

    return true;
  }
}
