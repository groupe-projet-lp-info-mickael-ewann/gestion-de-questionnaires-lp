<?php

namespace App\Controller;

use App\Entity\PremadeAnswer;
use App\Entity\Quiz;
use App\Entity\QuizQuestion;
use App\Repository\PremadeAnswerRepository;
use App\Repository\QuestionTypeRepository;
use App\Repository\QuizQuestionRepository;
use App\Repository\QuizRepository;
use App\Repository\SubjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use function PHPSTORM_META\elementType;

class QuizController extends AbstractController
{
    #[Route('/quizs', name: 'app_quiz')]
    public function index(QuizRepository $quizRepository): Response
    {
        return $this->render('quizs/index.html.twig', [
            'quizs' => $quizRepository->findAll(),
        ]);
    }

    #[Route('/quizs/{id}', name: 'app_quiz_single', requirements: ['id' => '\d+'])]
    public function single(
        QuizRepository $quizRepository,
        $id,
    ): Response {

        $quiz = $quizRepository->find($id);
        $quizQuestions = $quiz->getQuizQuestions();
        $premadesAnswers = array();
        foreach ($quizQuestions as $key => $value) {
            array_push($premadesAnswers, $value->getPremadeAnswers());
        }
        return $this->render('quizs/single.html.twig', [
            'quiz' => $quiz,
            'questions' => $quizQuestions,
            'answers' => $premadesAnswers,
        ]);
    }

    #[Route('/quizs/create', name: 'app_quiz_create')]
    public function create(): Response
    {
        return $this->render('quizs/create-edit.html.twig', []);
    }

    #[Route('/quizs/{id}/edit', name: 'app_quiz_edit_data')]
    public function edit($id): Response
    {
        return $this->render('quizs/create-edit.html.twig', ['id' => $id]);
    }


    #[Route('/quizs/{id}/delete', name: 'app_quiz_delete_data')]
    public function delete($id, EntityManagerInterface $entityManager, QuizRepository $quizRepository): Response
    {

        $entityManager->remove($quizRepository->find($id));
        $entityManager->flush();
        $responseObject = (object) ['message' => 'jai reçu'];
        return $this->json($responseObject);
    }

    #[Route('/quizs/{id}/update', name: 'app_quiz_update_data')]
    public function update(LoggerInterface $logger, Request $request, QuizRepository $quizRepository,
    SubjectRepository $subjectRepository, EntityManagerInterface $entityManager, QuestionTypeRepository $questionTypeRepository,
    QuizQuestionRepository $quizQuestionRepository , PremadeAnswerRepository $premadeAnswerRepository
    
    ): Response
    {



      

        $logger->notice("Dans update");

        $parameters = json_decode($request->getContent(), true);
        // $logger->notice($parameters['label']);
        // $logger->notice($parameters['id']);
        $logger->notice($parameters['subject']);

        $quiz = $quizRepository->find($parameters['id']);


        $quiz->setQuizLabel($parameters['label']);
        $quiz->setSubject( $subjectRepository->find($parameters['subject']));

        

        

        $arrayQuestions = $parameters['questions'];

        foreach ($arrayQuestions as $questionKey => $questionValue) {
            $logger->notice("interation of foreach question");


            if($questionValue['id'] > 0){
                $questionEntity = $quizQuestionRepository->find($questionValue['id']);
            }else{
                $questionEntity = new QuizQuestion();
                $questionEntity->setRelatedQuiz($quiz);
                $entityManager->persist($questionEntity);
            }

          

            $questionEntity->setQuestionLabel($questionValue['label']);
            $questionEntity->setSubject( $subjectRepository->find( $questionValue['subject']));
            $questionEntity->setQuestionType( $questionTypeRepository->find( $questionValue['type']));
            // $logger->notice("Question Id is " . strval($questionValue['id']));
            // $logger->notice("Question Label is " . strval($questionValue['label']));
            // $logger->notice("Question subject is " . strval($questionValue['subject']));
            // $logger->notice("Question type is " . strval($questionValue['type']));



            foreach ($questionValue['answers'] as $answerKey => $answerValue) {


                if($answerValue['id'] > 0){
                    $answer = $premadeAnswerRepository->find($answerValue['id']);
                  
                }else{
                    $answer = new PremadeAnswer();
                    $answer->setRelatedQuestion($questionEntity);
                    $entityManager->persist($answer);
                }


                $answer->setAnswerLabel($answerValue['label']);
                $answer->setIsCorrect($answerValue['is-correct']);

                // $if($answerValue['id'] > 0){
                    // $answer = $premadeAnswerRepository->find($answerValue['id']);
                    // $answer->setAnswerLabel($answerValue['label']);
                    // $answer->setIsCorrect($answerValue['is-correct']);
                // }else{

                // }


                // $logger->notice("Answer Id is " . strval($answerValue['id']));
                // $logger->notice("Answer Label is " . strval($answerValue['label']));
                // $logger->notice("Answer isCorrect is " . strval($answerValue['is-correct']));



                // foreach ( $answerValue as $answerieldKey => $answerFieldValue){
                //     $logger->notice($answerieldKey);

                // }

            }
        }



        $arrayIdQ = $parameters['deleted-q-ids'];
        $arrayIdA = $parameters['deleted-a-ids'];

        foreach ($arrayIdA as $deletedKey => $deletedValue){
            $premadeAnswerToDelete = $premadeAnswerRepository->find($deletedValue);
            $entityManager->remove($premadeAnswerToDelete);
        }

        foreach ($arrayIdQ as $deletedKey => $deletedValue){
            $premadeQuestionToDelete = $quizQuestionRepository->find($deletedValue);
            $entityManager->remove($premadeQuestionToDelete);
        }

        /*
         $em=$this->getDoctrine()->getManager(); 
        $produitRepository=$em->getRepository(Produit::class); 
        $produit=$produitRepository->find($id); 
        $em->remove($produit); 
        $em->flush(); 
        $session=$request->getSession();  */

        $entityManager->flush();
        $responseObject = (object) ['message' => 'jai reçu'];
        return $this->json($responseObject);
    }


    #[Route('/quizs/add', name: 'app_quiz_add_data')]
    public function add(LoggerInterface $logger, Request $request, QuizRepository $quizRepository,
    SubjectRepository $subjectRepository, EntityManagerInterface $entityManager, QuestionTypeRepository $questionTypeRepository,
    QuizQuestionRepository $quizQuestionRepository , PremadeAnswerRepository $premadeAnswerRepository
    
    ): Response
    {



      

        $logger->notice("Dans add");

        $parameters = json_decode($request->getContent(), true);
        // $logger->notice($parameters['label']);
        // $logger->notice($parameters['id']);
        $logger->notice($parameters['subject']);

        $quiz = new Quiz();
        $entityManager->persist($quiz);

        $quiz->setQuizLabel($parameters['label']);
        $quiz->setSubject( $subjectRepository->find($parameters['subject']));

        

        

        $arrayQuestions = $parameters['questions'];

        foreach ($arrayQuestions as $questionKey => $questionValue) {
            $logger->notice("interation of foreach question");


          
                $questionEntity = new QuizQuestion();
                $questionEntity->setRelatedQuiz($quiz);
                $entityManager->persist($questionEntity);
            

          

            $questionEntity->setQuestionLabel($questionValue['label']);
            $questionEntity->setSubject( $subjectRepository->find( $questionValue['subject']));
            $questionEntity->setQuestionType( $questionTypeRepository->find( $questionValue['type']));
            // $logger->notice("Question Id is " . strval($questionValue['id']));
            // $logger->notice("Question Label is " . strval($questionValue['label']));
            // $logger->notice("Question subject is " . strval($questionValue['subject']));
            // $logger->notice("Question type is " . strval($questionValue['type']));



            foreach ($questionValue['answers'] as $answerKey => $answerValue) {


            
                    $answer = new PremadeAnswer();
                    $answer->setRelatedQuestion($questionEntity);
                    $entityManager->persist($answer);
                


                $answer->setAnswerLabel($answerValue['label']);
                $answer->setIsCorrect($answerValue['is-correct']);

                // $if($answerValue['id'] > 0){
                    // $answer = $premadeAnswerRepository->find($answerValue['id']);
                    // $answer->setAnswerLabel($answerValue['label']);
                    // $answer->setIsCorrect($answerValue['is-correct']);
                // }else{

                // }


                // $logger->notice("Answer Id is " . strval($answerValue['id']));
                // $logger->notice("Answer Label is " . strval($answerValue['label']));
                // $logger->notice("Answer isCorrect is " . strval($answerValue['is-correct']));



                // foreach ( $answerValue as $answerieldKey => $answerFieldValue){
                //     $logger->notice($answerieldKey);

                // }

            }
        }



        // $arrayIdQ = $parameters['deleted-q-ids'];
        // $arrayIdA = $parameters['deleted-a-ids'];

        // foreach ($arrayIdA as $deletedKey => $deletedValue){
        //     $premadeAnswerToDelete = $premadeAnswerRepository->find($deletedValue);
        //     $entityManager->remove($premadeAnswerToDelete);
        // }

        // foreach ($arrayIdQ as $deletedKey => $deletedValue){
        //     $premadeQuestionToDelete = $quizQuestionRepository->find($deletedValue);
        //     $entityManager->remove($premadeQuestionToDelete);
        // }

        /*
         $em=$this->getDoctrine()->getManager(); 
        $produitRepository=$em->getRepository(Produit::class); 
        $produit=$produitRepository->find($id); 
        $em->remove($produit); 
        $em->flush(); 
        $session=$request->getSession();  */

        $entityManager->flush();
        $responseObject = (object) ['message' => 'jai reçu'];
        return $this->json($responseObject);
    }

    #[Route('/quizs/{id}/fetch', name: 'app_quiz_fetch_data')]
    public function fetch(
        QuizRepository $quizRepository,
        $id,
    ): Response {

        $quiz = $quizRepository->find($id);
        $quizQuestions = $quiz->getQuizQuestions();
        $premadesAnswers = array();
        foreach ($quizQuestions as $key => $value) {
            array_push($premadesAnswers, $value->getPremadeAnswers());
        }

        $quizObject = (object) [
            'label' => $quiz->getQuizLabel(),
            'subject' => $quiz->getSubject()->getSubjectLabel(),
            'subject-id' => $quiz->getSubject()->getId()
        ];


        $questionsObjectArray = array();

        foreach ($quizQuestions as $key => $questionValue) {

            $questionObject = (object) [
                'label' => $questionValue->getQuestionLabel(),
                'subject' => $questionValue->getSubject()->getSubjectLabel(),
                'subject-id' => $questionValue->getSubject()->getId(),
                'id' => $questionValue->getId()
            ];
            array_push($questionsObjectArray, $questionObject);
            $answerObjectArray = array();

            foreach ($questionValue->getPremadeAnswers() as $key => $answerValue) {
                $answerObject = (object) [
                    'label' => $answerValue->getAnswerLabel(),
                    'id' => $answerValue->getId()
                ];
                $answerObject->isCorrect = $answerValue->getIsCorrect();
                array_push($answerObjectArray, $answerObject);
            }

            $questionObject->type = $questionValue->getQuestionType()->getId();
            $questionObject->answers = $answerObjectArray;
        }

        $quizObject->questions = $questionsObjectArray;
        return $this->json($quizObject);
    }
}
