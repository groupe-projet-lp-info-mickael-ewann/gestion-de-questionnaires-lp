<?php

namespace App\Controller;

use App\Entity\QuizQuestion;
use App\Form\QuizQuestionType;
use App\Repository\QuizQuestionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Opportunity;
use App\Form\OpportunityType;
use App\Repository\ContactRepository;
use App\Repository\OpportunityRepository;



class QuizQuestionController extends AbstractController
{
    #[Route('/quiz/questions', name: 'app_quiz_questions')]
    public function index(QuizQuestionRepository $quizQuestionRepository): Response
    {
        return $this->render('quiz_questions/index.html.twig', ['questions' => $quizQuestionRepository->findAll(),]);
    }

   

    #[Route('/quiz/questions/{id}', name: 'app_quizz_question_single')]
    public function single(

        Request $request,
        QuizQuestionRepository $quizQuestionRepository,
        $id,
    ): Response {

        $quizQuestion = $quizQuestionRepository->find($id);

        // if ($request->isMethod('post')) {
        //     // your code
        // }


        return $this->render('quiz_questions/single.html.twig', [
            'question' => $quizQuestion,
            'answers' => $quizQuestion->getPremadeAnswers(),
        ]);
    }
}
