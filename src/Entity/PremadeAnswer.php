<?php

namespace App\Entity;

use App\Repository\PremadeAnswerRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PremadeAnswerRepository::class)]
class PremadeAnswer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $answerLabel = null;

    #[ORM\Column]
    private ?bool $isCorrect = null;

    #[ORM\ManyToOne(inversedBy: 'premadeAnswers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?QuizQuestion $relatedQuestion = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnswerLabel(): ?string
    {
        return $this->answerLabel;
    }

    public function setAnswerLabel(string $answerLabel): self
    {
        $this->answerLabel = $answerLabel;

        return $this;
    }

    public function getIsCorrect(): ?bool
    {
        return $this->isCorrect;
    }

    public function setIsCorrect(bool $isCorrect): self
    {
        $this->isCorrect = $isCorrect;

        return $this;
    }

    public function getRelatedQuestion(): ?QuizQuestion
    {
        return $this->relatedQuestion;
    }

    public function setRelatedQuestion(?QuizQuestion $relatedQuestion): self
    {
        $this->relatedQuestion = $relatedQuestion;

        return $this;
    }
}
