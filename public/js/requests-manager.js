let _buttonUpdateQuizz;
let _quizId


let _idsOfDeletedQuestions = []
let _idsOfDeletedAnswers = []
let _dataHolder
document.addEventListener("DOMContentLoaded", () => {
  // testFetch();
  _dataHolder = document.getElementById("data-holder");
  _buttonUpdateQuizz = document.getElementById("create-update-quiz");

  _quizId = _dataHolder.dataset.quizId;

  if(_quizId > 0){
    _buttonUpdateQuizz.addEventListener("click",updateQuiz);

  }else{

    _buttonUpdateQuizz.addEventListener("click",postQuiz);

  }

  fetchQuizCategoriesFromURL();  




});

function fetchQuizCategoriesFromURL() {
  //Prevent normal form submission
  //Create url from id stored in dataset of form
  let url = "/subjects/fetch";
  //Get value to send in the request
  // let valueToUse = evt.currentTarget.value;
  // const dataForFullUpdate = {
  //   priority: valueToUse,
  // };
  //Send request
  getQuizJSON(url /*, dataForFullUpdate*/)
    .then((data) => {
      console.log(data);

      data.forEach((element) => {
        _allQuestionCategories[element.key] = element.label;
      });

      populateSelectWithKeyValueArray(
        "edit-question-category-select",
        _allQuestionCategories
      );

      populateSelectWithKeyValueArray(
        "quiz-category-select",
        _allQuestionCategories
      );




      if(_quizId > 0){
        fetchQuizFromURL();

      }else{
        _quizCategoryIndex ="1";
        updateQuizCategoryIndex();
      }

    })
    .catch((err) => {
      console.log('error in "updateTodoPriority" : ' + err);
    });
}

function fetchQuizFromURL() {
 

  let url = "/quizs/" + _quizId + "/fetch";
  console.log(url);

  //Send request
  getQuizJSON(url /*, dataForFullUpdate*/)
    .then((data) => {
      console.log(data);

      _quizCategorySelect.value = data["subject-id"].toString();
      _quizCategoryIndex = parseInt(data["subject-id"]);

      _titleEditInput.value = data["label"];

      let arrayOfQuestions = data["questions"];

      arrayOfQuestions.forEach((questionData) => {
        let newQuestion = new Question(
          parseInt(questionData["id"]),
          questionData["subject-id"].toString(),
          questionData["type"].toString(),
          questionData["label"]
        );

        let arrayOfAnswers = questionData['answers'];

          arrayOfAnswers.forEach(answerData => {
          
            let newAnswer = new Answer(answerData['id'], answerData['label'], answerData['isCorrect'] );
            newQuestion.addAnswer(newAnswer);

        });
        

        _allQuestions.push(newQuestion);

        updateQuizCategoryIndex();
      });

      displayAllQuestionsInPreview();
    })
    .catch((err) => {
      console.log('error in "updateTodoPriority" : ' + err);
    });
}

async function getQuizJSON(url, data) {
  const response = await fetch(url, {
    method: "GET",
    headers: {
      "Content-type": "application/json",
    },
    /* body: JSON.stringify(data),*/
  });
  const responseData = await response.json();
  return responseData;
}


function canSubmitQuiz(){

  _allQuestions.forEach(question => {
    if(question.canBeSubmited == false){
      console.error("can't submit");
      return false;
    }
  });

  return true;

}

function updateQuiz(){
  

  if(! canSubmitQuiz()){

    console.error("return");
    return;
  }

  let dataToUse = new Object();
  dataToUse['label'] = _titleEditInput.value;
  dataToUse['id'] = parseInt(_quizId);
  dataToUse['subject'] = parseInt( _quizCategorySelect.value);
  
  let allQuestions = [];

  _allQuestions.forEach(question => {
    let questionObj = {};
    questionObj['id'] = question.id;
    questionObj['label'] = question.label;
    questionObj['subject'] = parseFloat(question.questionCategory);
    questionObj['type'] = parseFloat(question.questionType);

    let answersArray = [];

    question.answers.forEach(answer => {
      let answerObj = {};
      answerObj['id'] = answer.id;
      answerObj['label'] = answer.label;
      answerObj['is-correct'] = answer.isCorrect;
      answersArray.push(answerObj);
    });
    questionObj['answers'] = answersArray;

    allQuestions.push(questionObj);
  });

  dataToUse['questions'] = allQuestions;
  dataToUse['deleted-q-ids'] = _idsOfDeletedQuestions
  dataToUse['deleted-a-ids'] = _idsOfDeletedAnswers


  url ="/quizs/"+_quizId+"/update";

//   console.log(JSON.stringify(dataToUse, null, 2))

// return;


  putUpdatedQuizJSON(url, dataToUse).then((data) => {
    console.log(data);
    console.log(data['message']);
    console.log(data['message'].toString());
    location.href='/quizs/'+_quizId;

   // displayAllQuestionsInPreview();
  })
  .catch((err) => {
    console.log('error in "update quiz" : ' + err);
  });
 


}


function postQuiz(){
  if(! canSubmitQuiz()){
    return;
  }
  console.clear()
  let dataToUse = new Object();
  dataToUse['label'] = _titleEditInput.value;
  dataToUse['id'] = parseInt(_quizId);
  dataToUse['subject'] = parseInt( _quizCategorySelect.value);
  
  let allQuestions = [];

  _allQuestions.forEach(question => {
    let questionObj = {};
    questionObj['id'] = question.id;
    questionObj['label'] = question.label;
    questionObj['subject'] = parseFloat(question.questionCategory);
    questionObj['type'] = parseFloat(question.questionType);

    let answersArray = [];

    question.answers.forEach(answer => {
      let answerObj = {};
      answerObj['id'] = answer.id;
      answerObj['label'] = answer.label;
      answerObj['is-correct'] = answer.isCorrect;
      answersArray.push(answerObj);
    });
    questionObj['answers'] = answersArray;

    allQuestions.push(questionObj);
  });
  dataToUse['questions'] = allQuestions;
 


  url ="/quizs/add";

//   console.log(JSON.stringify(dataToUse, null, 2))

// return;


postQuizJSON(url, dataToUse).then((data) => {
    console.log(data);
    console.log(data['message']);
    console.log(data['message'].toString());
    location.href='/quizs/';
   // displayAllQuestionsInPreview();
  })
  .catch((err) => {
    console.log('error in "update quiz" : ' + err);
  });
 


}



async function putUpdatedQuizJSON(url, data) {
  console.log(JSON.stringify(data, null, 2))
  const response = await fetch(url, {
    method: "PUT",
    headers: {
      "Content-type": "application/json",
    },
      body: JSON.stringify(data),
     // body: data,
  });
  // console.log("post "+ await response)
  // let bodyJson = await response.json();
  // console.log("post "+ bodyJson);
  const responseData = await response.json();
  return responseData;
}



async function postQuizJSON(url, data) {
  console.log(JSON.stringify(data, null, 2))
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
      body: JSON.stringify(data),
  });
  const responseData = await response.json();
  return responseData;
}
