<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230207132131 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__quiz AS SELECT id, quiz_label FROM quiz');
        $this->addSql('DROP TABLE quiz');
        $this->addSql('CREATE TABLE quiz (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, subject_id INTEGER NOT NULL, quiz_label VARCHAR(255) NOT NULL, CONSTRAINT FK_A412FA9223EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO quiz (id, quiz_label) SELECT id, quiz_label FROM __temp__quiz');
        $this->addSql('DROP TABLE __temp__quiz');
        $this->addSql('CREATE INDEX IDX_A412FA9223EDC87 ON quiz (subject_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__quiz AS SELECT id, quiz_label FROM quiz');
        $this->addSql('DROP TABLE quiz');
        $this->addSql('CREATE TABLE quiz (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, quiz_label VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO quiz (id, quiz_label) SELECT id, quiz_label FROM __temp__quiz');
        $this->addSql('DROP TABLE __temp__quiz');
    }
}
