let _rootPreview;
let _rootEdit;

let _editQuestionFielsDivWrapper;

/*edit-question-field-div-wrapper */

let _alertSystemWrapper;
let _alertContainer;

let _editQuestionLabelInput;
let _editQuestionOrderDisplay;
let _editQuestionCategorySelect;
let _editQuestionTypeSelect;

let _quizCategorySelect;

let _titleEditInput;

// let _allQuestionCategories = [
//   "Maths",
//   "Français",
//   "Anglais",
//   "PHP",
//   "C#",
//   "Java",
// ];

let _allQuestionCategories = {

}

// let _allQuestionTypes = ["QCM - Réponse unique",
// "QCM - Réponses multiples",
// "Nombre",
// "Réponse courte",
// "Réponse ouverte"];

let _allQuestionTypes = {
  1: "QCM-Unique",
  2: "QCM-Multiple",
  3: "Nombre",
  4: "Rép. Courte",
  5: "Rép. Longue",
};

let _tempNewAnswers = [];

let _quizCategoryIndex = 0;

let _currentlyEditedQuestionInstance = null;

let _addAnswerButton;

let _buttonValidateQuestion;

let _allQuestions = new Array();

document.addEventListener("DOMContentLoaded", () => {
  _rootEdit = document.getElementById("edit-root");
  _rootPreview = document.getElementById("preview-root");
  _editQuestionLabelInput = document.getElementById(
    "edit-question-label-input"
  );
  _editQuestionOrderDisplay = document.getElementById(
    "edit-question-order-display"
  );
  _editQuestionCategorySelect = document.getElementById(
    "edit-question-category-select"
  );
  _editQuestionTypeSelect = document.getElementById(
    "edit-question-type-select"
  );

  _editQuestionFielsDivWrapper = document.getElementById(
    "edit-question-fields-div-wrapper"
  );

  _quizCategorySelect = document.getElementById("quiz-category-select");

  const newQuestionButton = document.getElementById("btn-new-question");
  newQuestionButton.addEventListener("click", addNewQuestionAtTheEnd);

  _addAnswerButton = document.getElementById("btn-add-answer");
  _addAnswerButton.addEventListener("click", addNewAnswer);

  const quizCategorySelect = document.getElementById("quiz-category-select");
  quizCategorySelect.addEventListener("change", updateQuizCategoryIndex);

  _titleEditInput = document.getElementById("title-edit-input");

  populateSelectWithKeyValueArray(
    "edit-question-type-select",
    _allQuestionTypes
  );

  _editQuestionTypeSelect.value = 1;



  _buttonValidateQuestion = document.getElementById(
    "btn-apply-question-changes"
  );
  _buttonValidateQuestion.addEventListener("click", applyQuestionChanges);

  console.log("btn validate is null " + (_buttonValidateQuestion == null));

  // const questionTypeSelect = document.getElementById("edit-question-type-select");
  // _editQuestionTypeSelect.addEventListener("change", function() {updateDisplayedQuestionInEdit(_currentlyEditedQuestionInstance);});
  _editQuestionTypeSelect.addEventListener(
    "change",
    processChangeOfQuestionType
  );

 // addNewQuestionAtTheEnd();

//  updateQuizCategoryIndex();

  _alertSystemWrapper = document.getElementById("alert-system-wrapper");
  _alertContainer = document.getElementById("alert-container");



  _currentlyEditedQuestionInstance  = null;

});

function addNewAnswer() {
  console.log("add new answer test");
  addNewAnswerToTempArray();
}

function applyQuestionChanges() {

  if(_currentlyEditedQuestionInstance == null){
    return;
  }
  _currentlyEditedQuestionInstance.overrideQuestionCategory(
    _editQuestionCategorySelect.value
  );
  _currentlyEditedQuestionInstance.overrideQuestionType(
    _editQuestionTypeSelect.value
  );
  _currentlyEditedQuestionInstance.overrideLabel(_editQuestionLabelInput.value);

  //remove all existing answers
  _currentlyEditedQuestionInstance.removeAllAnswers();

  // let allAnswersTextValue = document.getElementsByClassName("answer-label-edit");
  let allAnswersTextValue = [
    ...document.getElementsByClassName("answer-label-edit"),
  ];
  let allAnswersCheckbox = [
    ...document.getElementsByClassName("answer-checkbox-edit"),
  ];

  for (let index = 0; index < _tempNewAnswers.length; index++) {
    const newAnswer = _tempNewAnswers[index];
    let textInputToUse = allAnswersTextValue.find(
      (x) => x.id === "answer-label-edit-" + index
    );
    // let textInputToUse = allAnswersTextValue.getElementById("answer-label-edit-"+index);
    let checkboxInputToUse = allAnswersCheckbox.find(
      (x) => x.id === "answer-checkbox-edit-" + index
    );
    // let checkboxInputToUse = allAnswers.getElementById("answer-checkbox-edit-"+index);

    newAnswer.overrideLabel(textInputToUse.value);
    newAnswer.overrideIsCorrect(checkboxInputToUse.checked);
    _currentlyEditedQuestionInstance.addAnswer(newAnswer);
  }

  _currentlyEditedQuestionInstance = null;
  _tempNewAnswers = new Array();
  updateDisplayedQuestionInEdit();
  displayAllQuestionsInPreview();

  //clear display
  displayAllAnswersInEdit();
}

//read the value of the category selected and store in a script scope variable

/**
 * Read the value of the category selected and store in the script scope variable { _quizCategoryIndex }.
 */
function updateQuizCategoryIndex() {
  //get the index, using the appropriate function with the category select as an argument
  //and assign it to the variable
  _quizCategoryIndex = _quizCategorySelect.value;
  //TO REMOVE AT END : check if read is correct during tests
  console.log("index of quiz category is " + _quizCategoryIndex);
}


/**
 * @param {object}  select The select element to read.
 * @returns The index of the currently selected option.
 */
function getIndexOfSelect(select) {
  //get all options which are children of the given index
  optionsToIterate = select.getElementsByTagName("option");

  //iterate through all options
  for (let index = 0; index < optionsToIterate.length; index++) {
    const optionCurrent = optionsToIterate[index];
    //if selected, return the index
    if (optionCurrent.selected) {
      return index;
    }
  }
}

// }



function populateSelectWithKeyValueArray(id, object) {
  //get the required select using the given id
  let selectToPopulate = document.getElementById(id);

  for (const key in object) {
    if (Object.hasOwnProperty.call(object, key)) {
      const currentLabel = object[key];
      //store the string value of the current index of the array in a local variable
      //   const currentLabel = array[index];
      //create an option element and its text node child, using the label, then append them as required
      let optionElement = document.createElement("option");
      let optionElementLabel = document.createTextNode(currentLabel);
      optionElement.appendChild(optionElementLabel);
      selectToPopulate.appendChild(optionElement);
      //use the label to set the value attribute of the option
      optionElement.value = key;
    }
  }
}

/**
 * - Creation of a new question, added at the end of the question array, then refresh display
 * - Creation d'une nouvelle question, l'ajoute à la fin de l'array des questions puis actualise l'affichage
 */
function addNewQuestionAtTheEnd() {
  //set the type of the new question : if there is at least another question, copy its type, else use 0
  //(since type are stored as indexes and not as values)
  let typeToUse =
    _allQuestions.length > 0
      ? _allQuestions[_allQuestions.length - 1].questionType
      : 1;
  let newQuestion = new Question(
    -1,
    _quizCategoryIndex,
    typeToUse,
    "Lorem Ipsum",
    _allQuestions.length
  );
  //add created question at the end of the array
  _allQuestions.push(newQuestion);
  //display the newly created question
  setCurrentlyEditedQuestionInstance(newQuestion);

  displayAllQuestionsInPreview();
}

function addNewAnswerToTempArray() {
  //
  if (_currentlyEditedQuestionInstance == null) {
    console.error("Tried to add an Answer to null Question");
    return;
  }

  console.log("ADD NEW ANSWER");
  let newAnswer = new Answer(-1, "Lorem Ipsum Answer", _currentlyEditedQuestionInstance.questionType =="4" ? true : false);
  //add created question at the end of the array
  _tempNewAnswers.push(newAnswer);
  //display the newly created question

  // displayAllAnswersInEdit();
  createAnswerDisplayInEdit(
    newAnswer,
    _currentlyEditedQuestionInstance,
    _tempNewAnswers.length - 1
  );
}

function displayAllAnswersInEdit() {
  //clear the displayed answers and redraw all
  _rootEdit.innerHTML = "";

  if (_currentlyEditedQuestionInstance == null) {
    console.log("Tried to display Answers to null Question");
    return;
  }

  for (let index = 0; index < _tempNewAnswers.length; index++) {
    const tempAnswer = _tempNewAnswers[index];
    console.log("must draw an answer from temp");
    createAnswerDisplayInEdit(
      tempAnswer,
      _currentlyEditedQuestionInstance,
      index
    );
  }
}

function createAnswerDisplayInEdit(answer, question, index) {
  console.log(
    "Create answer for a question of type" +
      _allQuestionTypes[question.questionType]
  );


  let isNumber = _editQuestionTypeSelect.value == "3";
  console.log("is number "+ isNumber + " "+_editQuestionTypeSelect.value);

  let divAnswer = document.createElement("div");
  divAnswer.id = "answer-display-edit-" + index;

  let divMainPart = document.createElement("div");

  divAnswer.appendChild(divMainPart);

  // let answerName = document.createElement("p");

  // let answerNameLabel = document.createTextNode("My Answer");
  // answerName.appendChild(answerNameLabel);

  //create an input that will work as an image-button, input work better than image for the wanted style
  let buttonDeleteButton = document.createElement("input");
  //attach it to the parent div
 
  if(!isNumber){
    divAnswer.appendChild(buttonDeleteButton);
  }

  divAnswer.classList.add("edit-answer-element");

  divMainPart.classList.add("edit-answer-content");

  switch (question.questionType.toString()) {
    case "1":
    case "2":
    case "3":
    case "4":
      tempLabel = document.createElement("label");
      tempLabelText = document.createTextNode( isNumber ? index == 0 ?  "Nombre : " : "Margeeeee : " : "Réponse : ");
      let tempInputText = document.createElement("input");
      tempInputText.type = isNumber ? "number" : "text";
      tempInputText.id = "answer-label-edit-" + index;
      tempInputText.classList.add("answer-label-edit");
      tempInputText.value = answer.label;



      tempLabel.appendChild(tempLabelText);
      tempLabel.appendChild(tempInputText);

      divMainPart.appendChild(tempLabel);
      

      

      let tempCheckboxLabel = document.createElement("label");
      tempCheckboxLabel.classList.add("checkbox-label");
      let tempCheckboxLabelText = document.createTextNode("Correcte : ");
      let tempInputCheckbox = document.createElement("input");
      tempInputCheckbox.type = "checkbox";
      tempInputCheckbox.id = "answer-checkbox-edit-" + index;
      tempInputCheckbox.classList.add("answer-checkbox-edit");
      tempInputCheckbox.checked = isNumber ? index == 0 ? true : false : answer.isCorrect;
      tempInputCheckbox.readOnly = isNumber;
      
     
      if(_editQuestionTypeSelect.value.toString() ==  "4"){
        tempCheckboxLabel.style.display = 'none';
      }
      tempInputCheckbox.hidden = (_editQuestionTypeSelect.value.toString() ==  "4")
      console.log("je veux u ntrue "+(_editQuestionTypeSelect.value.toString() ==  "4"))

      tempCheckboxLabel.appendChild(tempCheckboxLabelText);
      tempCheckboxLabel.appendChild(tempInputCheckbox);
      divMainPart.appendChild(tempCheckboxLabel);

    
     

      break;
    default:
      console.error(
        "ne devrait pas arriver ici lors du test " + question.questionType
      );
      break;
  }

  buttonDeleteButton.classList.add("btn-delete");
  //set the type of input of the delete button as image in order to display it correctly
  buttonDeleteButton.type = "image";
  //set the path to the image to use for the delete button
  buttonDeleteButton.src = "/images/trash.png";

  _rootEdit.appendChild(divAnswer);

  buttonDeleteButton.addEventListener("click", deleteAnswer);
  buttonDeleteButton.indexOfQuestionWhereAnswerIsFrom = _allQuestions.indexOf(question);
  buttonDeleteButton.indexOfAnswer = question.answers.indexOf(answer);

}

/**
 * - Clear the content of the root element of the preview section, then populate it with question previews
 * - Vide le contenu de l'élément racine de la section de prévisualisation, puis génère en enfant des prévisualisations des questions
 */
function displayAllQuestionsInPreview() {
  //clear any preview HTML content of the root before creating the new preview
  _rootPreview.innerHTML = "";
  //iterate through the question array...
  for (const question of _allQuestions) {
    //then call the individual question creator, with the current question as an argument
    createQuestionPreview(question);

    console.log(question);
  }
}

/**
 * - Create a question preview div and append it to the preview root element of the DOM, using the Question class instance given
 * - Créer une div de prévisualisation de question et l'attache à la racine des prévisualisation dans le DOM, en utilisant l'instance de la classe question donnée
 * @param {Question} question The instance to display as a preview
 */
function createQuestionPreview(question) {
  

  //create the parent div
  let buttonQuestion = document.createElement("div");
  //make it clickable
  buttonQuestion.addEventListener("click", logTestId);
  //make the listening function able to get the question order
  buttonQuestion.questionOrder = _allQuestions.indexOf( question);

  //create a div that will serve at a "sub-root", where all the main content of the preview will be appended
  let buttonQuestionContent = document.createElement("div");
  //attach it to the parent div
  buttonQuestion.appendChild(buttonQuestionContent);

  //create an input that will work as an image-button, input work better than image for the wanted style
  let buttonDeleteButton = document.createElement("input");
  //attach it to the parent div
  buttonQuestion.appendChild(buttonDeleteButton);

  //-parent_div
  //--sub_root
  //--delete_button

  //set the id of the parent div, with a common base and its order
  //the id is currently unused, but it is created just in case it will be needed later
  buttonQuestion.id = "preview-question-" + _allQuestions.indexOf( question);
  //set the class of the parent div, used only by css
  buttonQuestion.classList.add("preview-question-element");

  buttonDeleteButton.classList.add("btn-delete");
  //set the type of input of the delete button as image in order to display it correctly
  buttonDeleteButton.type = "image";
  //set the path to the image to use for the delete button
  buttonDeleteButton.src = "/images/trash.png";

  buttonQuestionContent.classList.add("preview-question-content");

  let pQuestionTitleLabel = document.createTextNode(
    "Question " +
      ( _allQuestions.indexOf(question) + 1) +
      " - " +
      question.questionCategoryText +
      " : " +
      question.label
  );

  let pQuestionTitle = document.createElement("p");
  pQuestionTitle.appendChild(pQuestionTitleLabel);

  // buttonQuestion.appendChild(pQuestionTitle);
  buttonQuestionContent.appendChild(pQuestionTitle);
  _rootPreview.appendChild(buttonQuestion);

  let needToDisplay = question.needToDisplayMessageAskingForMoreAnswers;

  if (needToDisplay) {
    let messageErrorElement = document.createElement("p");
    let messageErrorElementLabel = document.createTextNode(
      "Plus de réponses doivent être assignée à cette question."
    );
    messageErrorElement.classList.add("error");
    messageErrorElement.appendChild(messageErrorElementLabel);
    // buttonQuestion.appendChild(messageErrorElement);
    buttonQuestionContent.appendChild(messageErrorElement);
  }
  // else {
  //   let hasEnoughGoodAnswer = question.hasEnoughGoodAnswerIfQCM;
  // }

  let tempLabel;
  let tempLabelText;

  console.log("b4 switch " + question.questionType);
  switch (question.questionType) {
    case "1": //MCQ - 1 correct answer
    case "2":
      case "4":
      console.log("case 1 2");

      for (let index = 0; index < question.answers.length; index++) {
        const answer = question.answers[index];
        let tempParaA = document.createElement("p");
        tempParaA.classList.add("preview-answer");
        let tempParaAText = document.createTextNode(answer.label + " ");

        buttonQuestionContent.appendChild(tempParaA);
        tempParaA.appendChild(tempParaAText);

        console.error(answer.isCorrect);

        if(question.questionType != "4"){
          tempParaA.innerHTML += answer.isCorrect
          ? '<i class="fa-solid  fa-check correct"></i>'
          : '<i class="fa-solid  fa-xmark incorrect"></i>';
        }else{
          tempParaA.innerHTML = '<i class="fa-sharp fa-solid fa-check correct"></i> '+ tempParaA.innerHTML
        }
      

        //<i class="fa-solid  {{ (answer.getIsCorrect) ? 'fa-check' : 'fa-xmark' }}
        // {{ (answer.getIsCorrect) ? 'correct' : 'incorrect' }}"></i>
      }

      break;

    case "3":
      tempLabel = document.createElement("label");

      let numberToUse = question.answers[0].isCorrect ? question.answers[0].label : question.answers[1].label;
      let rangeToUse = question.answers[0].isCorrect == false ? question.answers[0].label : question.answers[1].label;
      tempLabelText = document.createTextNode("Réponse : "+numberToUse+"(+/-"+rangeToUse+")");
      tempLabel.appendChild(tempLabelText);
      buttonQuestionContent.appendChild(tempLabel);

      break;
   
      // tempLabel = document.createElement("label");
      // tempLabelText = document.createTextNode("Réponse : ");
      // let tempInputText = document.createElement("input");
      // tempInputText.type = "text";
      // tempInputText.readOnly = true;

      // tempLabel.appendChild(tempLabelText);
      // tempLabel.appendChild(tempInputText);
      // buttonQuestionContent.appendChild(tempLabel);

      break;

    case "5":
      tempLabel = document.createElement("label");
      tempLabelText = document.createTextNode("Réponse : ");
      let tempInputArea = document.createElement("textarea");
      tempInputArea.cols = 90;
      tempInputArea.rows = 2;
      tempInputArea.readOnly = true;

      tempLabel.appendChild(tempLabelText);
      // buttonQuestion.appendChild(tempLabel);
      // buttonQuestion.appendChild(tempInputArea);
      buttonQuestionContent.appendChild(tempLabel);
      buttonQuestionContent.appendChild(tempInputArea);
      //<textarea name="Text1" cols="40" rows="5"></textarea>

      break;

    default:
      break;
  }
  console.log("after switch");

  buttonDeleteButton.addEventListener("click", deleteQuestion);
  buttonDeleteButton.indexOfQuestionToDelete = _allQuestions.indexOf(question);

  console.log("test delete question order at step A " +  _allQuestions.indexOf(question));
}

function deleteQuestion(evt) {
  console.log("Delete step B " + evt.currentTarget.indexOfQuestionToDelete);
  evt.stopPropagation();
  _alertSystemWrapper.hidden = false;

  let newConfirmWindow = createConfirmWindow(
    "Demande de confirmation",
    "Voulez vous vraiment supprimer la question ?"
  );
  _alertContainer.appendChild(newConfirmWindow);

  newConfirmWindow
    .getElementsByClassName("confirm-button")[0]
    .addEventListener("click", confirmFromConfirmWindow);
  newConfirmWindow
    .getElementsByClassName("cancel-button")[0]
    .addEventListener("click", cancelFromConfirmWindow);
  newConfirmWindow.getElementsByClassName(
    "confirm-button"
  )[0].indexOfQuestionToDelete = evt.currentTarget.indexOfQuestionToDelete;
  newConfirmWindow.getElementsByClassName(
    "cancel-button"
  )[0].indexOfQuestionToDelete = evt.currentTarget.indexOfQuestionToDelete;
  // disableScrolling();


  unloadScrollBars();
}


function confirmFromConfirmWindow(evt) {
  console.log("delete final step " + evt.currentTarget.indexOfQuestionToDelete + "   questions lenght"+_allQuestions.length);
  if(_allQuestions[evt.currentTarget.indexOfQuestionToDelete].id != -1){
    _idsOfDeletedQuestions.push(_allQuestions[evt.currentTarget.indexOfQuestionToDelete].id)
  }
 
  _allQuestions.splice(evt.currentTarget.indexOfQuestionToDelete, 1);


 

  displayAllQuestionsInPreview();

  closeDeleteConfirmationWindow();
}


function cancelFromConfirmWindow() {
  closeDeleteConfirmationWindow();
}

function closeDeleteConfirmationWindow() {
  _alertContainer.innerHTML = "";
  _alertSystemWrapper.hidden = true;
  reloadScrollBars()

  // enableScrolling();
}

function deleteAnswer(evt) {
  let questionIndex = evt.currentTarget.indexOfQuestionWhereAnswerIsFrom;
  console.log("Delete answer from question " + questionIndex);


  evt.stopPropagation();

  if( _allQuestions[questionIndex].answers[evt.currentTarget.indexOfAnswer].id != -1 ){
    _idsOfDeletedAnswers.push(_allQuestions[questionIndex].answers[evt.currentTarget.indexOfAnswer].id)
  }

  _allQuestions[questionIndex].answers.splice(evt.currentTarget.indexOfAnswer, 1);

  displayAllAnswersInEdit();

  displayAllQuestionsInPreview();
 
}

//  A TESTER PLUS TARD
// too junky to be used
//  function disableScrolling(){
//     var x=window.scrollX;
//     var y=window.scrollY;
//     window.onscroll=function(){window.scrollTo(x, y);};
// }

// function enableScrolling(){
//     // window.onscroll=function(){};
//     window.onscroll = null;
// }

function createConfirmWindow(title, mainText) {
  let window = document.createElement("div");
  window.classList.add("alert-confirm");

  let windowTitle = document.createElement("p");
  windowTitle.classList.add("alert-title");
  windowTitle.appendChild(document.createTextNode(title));

  let windowMessage = document.createElement("p");
  windowMessage.classList.add("alert-message");
  windowMessage.appendChild(document.createTextNode(mainText));

  let confirmButton = document.createElement("button");
  confirmButton.classList.add("confirm-button");
  confirmButton.appendChild(document.createTextNode("Confirmer"));
  let cancelButton = document.createElement("button");
  cancelButton.classList.add("cancel-button");
  cancelButton.appendChild(document.createTextNode("Annuler"));

  window.appendChild(windowTitle);
  window.appendChild(windowMessage);
  window.appendChild(confirmButton);
  window.appendChild(cancelButton);

  return window;
}

function logTestId(evt) {
  const index = evt.currentTarget.questionOrder;
  console.log("Test " + evt.currentTarget.questionOrder);

  _rootEdit.innerHTML = "";
  setCurrentlyEditedQuestionInstance(_allQuestions[index]);
}

function setCurrentlyEditedQuestionInstance(question) {
  _currentlyEditedQuestionInstance = question;
  _tempNewAnswers = _currentlyEditedQuestionInstance.answers;
  updateDisplayedQuestionInEdit(question);
  displayAllAnswersInEdit();
}

function processChangeOfQuestionType() {
  let previousType = _currentlyEditedQuestionInstance.questionType;
  let newType = _editQuestionTypeSelect.value.toString();
  console.log("old value " + previousType);
  console.log("new value " + newType);

  switch (previousType) {
    case "1":
    case "2":
      switch (newType) {
        case "1":
        case "2":
          console.log("do nothing");
          break;
        case "3":
        case "4":
        case "5":
          console.log("reset");
          _tempNewAnswers = new Array();
          break;
      }

      break;
    default:
      console.log("reset");
      _tempNewAnswers = new Array();
      break;
  }

  if(newType == "3"){
    let newAnswer0 = new Answer(-1,"", true);
    let newAnswer1 = new Answer(-1,"", false);
    _tempNewAnswers.push(newAnswer0);
    _tempNewAnswers.push(newAnswer1);
  }

  updateDisplayOfAddAnswerButton();
}

function updateDisplayOfAddAnswerButton(/*question*/) {
  let questionTypeIndex = _editQuestionTypeSelect.value;
  _addAnswerButton.hidden =
    questionTypeIndex == "1" || questionTypeIndex == "2" || questionTypeIndex == "4" ? false : true;
  // _rootEdit.hidden = _addAnswerButton.hidden;
  console.log(
    "question type is 1 or 2 or 4 " +
      (questionTypeIndex == "1" || questionTypeIndex == "2" || questionTypeIndex == "4")
  );
  console.log("add answer is hidden " + (_addAnswerButton.hidden == null));
  //  question.questionType = questionTypeIndex;

  console.log(questionTypeIndex)

  displayAllAnswersInEdit();
}

function updateDisplayedQuestionInEdit(question) {
  console.log("test test update displayed");
  _editQuestionOrderDisplay.innerHTML = "";
  _editQuestionFielsDivWrapper.hidden = false;

  if (question != null) {
    _editQuestionLabelInput.value = question.label;
    _editQuestionFielsDivWrapper.hidden = false;
    _buttonValidateQuestion.hidden = false;
    _editQuestionOrderDisplay.appendChild(
      document.createTextNode("Question - " + ( _allQuestions.indexOf(question) +1 ))
    );
    _editQuestionCategorySelect.value = question.questionCategory;

    _editQuestionTypeSelect.value = question.questionType;
    console.log("q type " + question.questionType);

    //Todo : draw all already existing answers here with edition fields

    if (question.questionType == 1 || question.questionType == 2) {
      //draw
    }

    //Display button only if question must have premade answers.
    updateDisplayOfAddAnswerButton();
  } else {
    // question == null

    _editQuestionOrderDisplay.appendChild(
      document.createTextNode("Pas de question selectionnée")
    );
    _addAnswerButton.hidden = true;
    _editQuestionFielsDivWrapper.hidden = true;
    _buttonValidateQuestion.hidden = true;
  }
}

function consoleLogAll() {
  for (const quest of _allQuestions) {
    console.log(quest);
    quest.logAllAnswers();
  }
}




function reloadScrollBars() {
  document.documentElement.style.overflow = 'auto';  // firefox, chrome
  document.body.scroll = "yes"; // ie only
}

function unloadScrollBars() {
  document.documentElement.style.overflow = 'hidden';  // firefox, chrome
  document.body.scroll = "no"; // ie only
}