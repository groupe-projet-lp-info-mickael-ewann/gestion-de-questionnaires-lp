<?php

namespace App\Controller;

use App\Repository\SubjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SubjectController extends AbstractController
{
    #[Route('/subjects/fetch', name: 'app_subject_fetch')]
    public function index(
        SubjectRepository $subjectRepository
    ): Response
    {




        $allSubjects = $subjectRepository->findAll();

        $allObjects  = (array());

        foreach ($allSubjects as $key => $value) {

            $questionObject = (object) ['label' => $value->getSubjectLabel(),
            'key' => $value->getId()];
            array_push($allObjects, $questionObject);
            

        }

     

        
        return $this->json($allObjects);
    }
}
