<?php

namespace App\Command;

use App\Entity\PremadeAnswer;
use App\Entity\QuestionType;
use App\Entity\Quiz;
use App\Entity\QuizQuestion;
use App\Repository\QuestionTypeRepository;
use App\Repository\QuizRepository;
use App\Repository\QuizQuestionRepository;
use App\Repository\SubjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\MakerBundle\Validator;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[AsCommand(
    name: 'app:create-quiz',
    description: 'Create a quiz',
)]
class CreateQuizCommand extends Command
{


    private EntityManagerInterface $em;

    public function __construct(
        EntityManagerInterface $em,
        ValidatorInterface $vi,
        QuizRepository $quizRepo,
        SubjectRepository $subRepo,
        QuestionTypeRepository $questTypeRepo,

    ) {

        $this->em = $em;
        $this->vi = $vi;
        $this->quizRepo = $quizRepo;
        $this->subRepo = $subRepo;
        $this->questTypeRepo = $questTypeRepo;
        parent::__construct();
    }



    private QuizRepository $quizRepo;
    private SubjectRepository $subRepo;
    private QuestionTypeRepository $questTypeRepo;

    private ValidatorInterface $vi;



    protected function configure(): void
    {
        $this
            // ->addArgument('name', InputArgument::REQUIRED, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);




        $allSubjects = $this->subRepo->findAll();
        $allQuestionTypes = $this->questTypeRepo->findAll();


        $quizs = $this->quizRepo->findAll();

        if (count($quizs) > 0) {
            $io->text("All questions in database:");
            foreach ($quizs as $key => $value) {
                $io->text($value->getQuizLabel());
            }
        } else {
            $io->text("No quiz in database.");
        }

        $confirmCreate =   $io->confirm('Do you want to create a new quiz?', true);

        if ($confirmCreate) {
        } else {
            return Command::SUCCESS;
        }

        $labelToUseForQuiz =  $io->ask('Give a label for the new Quiz:');

        $allQuestions = array();

        $userFinishedAddingQuestions = false;

        while ($userFinishedAddingQuestions == false) {
            $labelToUseForQuestion =  $io->ask('Give a label for the new Question:');

            $allAnswers = array();

            $userFinishedAddingPremadeAnswers = false;

            while ($userFinishedAddingPremadeAnswers == false) {


                $answerLabel =  $io->ask('Give a label for the new PremadeAnswer:');



                $isCorrect =  $io->confirm('Is the answer supposed to be correct?', false);

                $answerToAdd = new PremadeAnswer();
                $answerToAdd->setAnswerLabel($answerLabel);
                $answerToAdd->setIsCorrect($isCorrect);

                array_push($allAnswers, $answerToAdd);
                $atLeastOneRightAnswer = false;
                $atLeastOneWrongAnswer = false;


                if (count($allAnswers) > 1) {
                    foreach ($allAnswers as $key => $value) {
                        $io->text($value->getIsCorrect());

                        if ($value->getIsCorrect()) {
                            $atLeastOneRightAnswer = true;
                        } else {
                            $atLeastOneWrongAnswer = true;
                        }
                    }

                    if ($atLeastOneRightAnswer && $atLeastOneWrongAnswer) {
                        $userFinishedAddingPremadeAnswers =  !$io->confirm('Do you want to add another answer?', false);
                    }
                }
            }





            $question = (new QuizQuestion)
                ->setQuestionLabel($labelToUseForQuestion)
                ->setSubject($allSubjects[array_rand($allSubjects, 1)])
                ->setQuestionType($allQuestionTypes[array_rand($allQuestionTypes, 1)]);
            array_push($allQuestions, $question);

            $this->em->persist($question);

            foreach ($allAnswers as $key => $value) {
                $value->setRelatedQuestion($question);
                $this->em->persist($value);
            }



            if (count($allQuestions) > 2) {
                $userFinishedAddingQuestions =  !$io->confirm('Do you want to add another question?', false);
            }
        }

        //



        $quiz = new Quiz();

        $quiz->setQuizLabel($labelToUseForQuiz);
        $quiz->setSubject($allSubjects[array_rand($allSubjects, 1)]);


        foreach ($allQuestions as $key => $value) {
            $value->setRelatedQuiz($quiz);
        }






        // $category = (new Category)->setCategoryLabel($labelToUseForQuestion);
        // $this->em->persist($category);


        //   dd($category);


        $this->em->persist($quiz);
        $this->em->flush();


        // $questions = $this->questionRepo->findAll();

        // $io->text("All questions in database:");
        // foreach ($questions as $key => $value) {
        //     $io->text($value->getQuestionLabel());
        // }



        return Command::SUCCESS;
    }
}
