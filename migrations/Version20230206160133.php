<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230206160133 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE premade_answer (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, related_question_id INTEGER NOT NULL, answer_label VARCHAR(255) NOT NULL, is_correct BOOLEAN NOT NULL, CONSTRAINT FK_D3EEA08BD5CC883B FOREIGN KEY (related_question_id) REFERENCES quiz_question (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_D3EEA08BD5CC883B ON premade_answer (related_question_id)');
        $this->addSql('CREATE TABLE quiz (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, quiz_label VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE quiz_question (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, related_quiz_id INTEGER NOT NULL, question_label VARCHAR(255) NOT NULL, CONSTRAINT FK_6033B00BBA251BD0 FOREIGN KEY (related_quiz_id) REFERENCES quiz (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_6033B00BBA251BD0 ON quiz_question (related_quiz_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE premade_answer');
        $this->addSql('DROP TABLE quiz');
        $this->addSql('DROP TABLE quiz_question');
    }
}
