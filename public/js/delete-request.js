


let _deleteButton;
document.addEventListener("DOMContentLoaded", () => {



    _deleteButton = document.getElementById("delete-btn");
    _deleteButton.addEventListener('click', askIfDeleteQuiz)


    _alertSystemWrapper = document.getElementById("alert-system-wrapper");
  _alertContainer = document.getElementById("alert-container");



});

function createConfirmWindow(title, mainText) {
  let window = document.createElement("div");
  window.classList.add("alert-confirm");

  let windowTitle = document.createElement("p");
  windowTitle.classList.add("alert-title");
  windowTitle.appendChild(document.createTextNode(title));

  let windowMessage = document.createElement("p");
  windowMessage.classList.add("alert-message");
  windowMessage.appendChild(document.createTextNode(mainText));

  let confirmButton = document.createElement("button");
  confirmButton.classList.add("confirm-button");
  confirmButton.appendChild(document.createTextNode("Confirmer"));
  let cancelButton = document.createElement("button");
  cancelButton.classList.add("cancel-button");
  cancelButton.appendChild(document.createTextNode("Annuler"));

  window.appendChild(windowTitle);
  window.appendChild(windowMessage);
  window.appendChild(confirmButton);
  window.appendChild(cancelButton);

  return window;
}

function askIfDeleteQuiz(evt) {
  evt.stopPropagation();
  _alertSystemWrapper.hidden = false;

  let newConfirmWindow = createConfirmWindow(
    "Demande de confirmation",
    "Voulez vous vraiment supprimer le quiz ?"
  );
  _alertContainer.appendChild(newConfirmWindow);

  newConfirmWindow
    .getElementsByClassName("confirm-button")[0]
    .addEventListener("click", confirmFromConfirmWindow);
  newConfirmWindow
    .getElementsByClassName("cancel-button")[0]
    .addEventListener("click", cancelFromConfirmWindow);
    unloadScrollBars();
}


function confirmFromConfirmWindow(evt) {
 

  deleteQuizAfterConfirmation();

  closeDeleteConfirmationWindow();
}


function cancelFromConfirmWindow() {
  closeDeleteConfirmationWindow();
}

function closeDeleteConfirmationWindow() {
  _alertContainer.innerHTML = "";
  _alertSystemWrapper.hidden = true;
  reloadScrollBars()

  // enableScrolling();
}




function deleteQuizAfterConfirmation(){
    let url = ""+_quizId+"/delete"
    console.log(url)

    deleteQuizAtId(url).then((data) => {
        
         location.href='/quizs';
    }).catch((err) => {
        console.log('error in "addEventListener" : ' + err);
      });


}

async function deleteQuizAtId(url) {
    const response = await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
      },
    });
    const responseData = await response.json();
    return responseData;
  }


  function reloadScrollBars() {
    document.documentElement.style.overflow = 'auto';  // firefox, chrome
    document.body.scroll = "yes"; // ie only
  }
  
  function unloadScrollBars() {
    document.documentElement.style.overflow = 'hidden';  // firefox, chrome
    document.body.scroll = "no"; // ie only
  }