function GQChoixMultiple(question, NumNom, rep) {
    let result = "";
    result = result + "::" + NumNom + ":: " + question;
    if (rep.length < 2 || rep.length > 5) {
        console.log("tableau de réponse de mauvaise taille");
        return;
    }
    else {
        result += "\n{";
        rep.forEach(reponse => {
            // result+=reponse.string+" ";
            if (reponse.isCorrect) {
                result += "=" + reponse.label + " ";
            }
            else if (!reponse.isCorrect) {
                result += "~" + reponse.label + " ";
            }
            else { console.log("error"); }
        });
        result += "}\n";
    }
    return result;
}
function GQouverte(question, NumNom) {
    let result = "";
    result = result + "::" + NumNom + ":: " + question + " {}\n";
    return result;
}
function GQChoixMultipleP(question, NumNom, rep) {
    let result = "";
    result = result + "::" + NumNom + ":: " + question;
    if (rep.length < 2 || rep.length > 5) {
        console.log("tableau de réponse de mauvaise taille");
        return;
    }
    else {
        result += "\n{\n";
        let nbCorrect = 0;

        rep.forEach(reponse => {
            if (reponse.isCorrect) nbCorrect++;
        });

        nbCorrect = 100 / nbCorrect;
        nbCorrect = Math.round(nbCorrect * 100000) / 100000;
        rep.forEach(reponse => {
            // result+=reponse.string+" ";
            if (reponse.isCorrect) {
                result += "~%" + nbCorrect + "%" + reponse.label + "\n";
            }
            else if (!reponse.isCorrect) {
                result += "~%-50%" + reponse.label + "\n";

            }
            else { console.log("error"); }

        });
        result += "}\n";
    }
    return result;

}
function GQcourteString(question, NumNom, rep) {
    let result = "";
    result = result + "::" + NumNom + ":: " + question + " {";
    rep.forEach(reponse => {
        result += " =" + reponse.label;
    });
    result += "}\n";
    return result;
}
function GQcourteNombre(question, NumNom, rep) {
    let result = "";
    if ((typeof rep) != "object") {
        console.error("reponse n'est pas un tableaux");
        return;
    }
    else if (rep.length != 2) {
        console.error("le tableau reponse contient le mauvais nombre de réponse");
        return;
    }
    else if (isNaN(rep[0].label)) {
        console.error("la réponse n'est pas un nombre");
        return;
    }
    else if (rep[0].isCorrect == rep[1].isCorrect) {
        console.error("les 2 reponses ont le meme bool impossible de différencier la marge de la valeur");
        return;
    }
    else {
        if (rep[0].isCorrect == true) {
            result = result + "::" + NumNom + ":: " + question + " {#";
            result += "\n=%100%" + rep[0].label + "\n=%50%" + rep[0].label + ":" + rep[1].label;
            result += "\n}\n";
            return result;
        }
        else {
            result = result + "::" + NumNom + ":: " + question + " {#";
            result += "\n=%100%" + rep[1].label + "\n=%50%" + rep[1].label + ":" + rep[0].label;
            result += "\n}\n";
            return result;
        }

    }
}

function questionG(questionObject, NumNom) {
    let result = "";
    switch (questionObject.type) {
        case 1:
            result = GQChoixMultiple(questionObject.label, NumNom, questionObject.answers);

            break;
        case 5:
            result = GQouverte(questionObject.label, NumNom);

            break;
        case 2:
            result = GQChoixMultipleP(questionObject.label, NumNom, questionObject.answers);

            break;
        case 4:
            result = GQcourteString(questionObject.label, NumNom, questionObject.answers);

            break;
        case 3:
            result = GQcourteNombre(questionObject.label, NumNom, questionObject.answers);
            break;

        default:
            break;
    }
    return result;
}
function quizzGift(quizz) {
    let result = "";
    quizz.questions.forEach(quest => {
        if (quest.subject == undefined || quest.subject == null) {
            result += questionG(quest, quizz.label + (quizz.questions.indexOf(quest) + 1).toString()).toString() + "\n";
        }
        else {
            result += "\n$CATEGORY: " + quest.subject + "\n\n" + questionG(quest, quizz.label + (quizz.questions.indexOf(quest) + 1).toString()).toString() + "\n";
        }
    });
    return result;

}


function XQChoixMultiple(question, NumNom, rep) {
    let result = "";

    if (rep.length < 2 || rep.length > 5) {
        console.log("tableau de réponse de mauvaise taille");
        return;
    }
    else {
        result += '\n<question type="multichoice">' +
            '\n<name>' +
            '\n<text>' + NumNom + '</text>' +
            '\n</name>' +
            '\n<questiontext format="moodle_auto_format">' +
            '\n<text>' + question + '</text>' +
            '\n</questiontext>' +
            '\n<generalfeedback format="moodle_auto_format">' +
            '\n<text></text>' +
            '\n</generalfeedback>'
        '\n<defaultgrade>1.0000000</defaultgrade>' +
            '\n<penalty>0.3333333</penalty>' +
            '\n<hidden>0</hidden>' +
            '\n<idnumber></idnumber>' +
            '\n<single>true</single>' +
            '\n<shuffleanswers>true</shuffleanswers>' +
            '\n<answernumbering>abc</answernumbering>' +
            '\n<showstandardinstruction>0</showstandardinstruction>' + '\n<correctfeedback format="moodle_auto_format">' +
            '\n<text></text>' +
            '\n</correctfeedback>' +
            '\n<partiallycorrectfeedback format="moodle_auto_format">' +
            '\n<text></text>' +
            '\n</partiallycorrectfeedback>' +
            '\n<incorrectfeedback format="moodle_auto_format">' +
            '\n<text></text>' +
            '\n</incorrectfeedback>';
        rep.forEach(reponse => {
            // result+=reponse.string+" ";
            if (reponse.isCorrect) {
                result += '\n<answer fraction="100" format="moodle_auto_format">' +
                    '\n<text>' + reponse.label + '</text>' +
                    '\n<feedback format="moodle_auto_format">' +
                    '\n<text></text>' +
                    '\n</feedback>' +
                    '\n</answer>';
            }
            else if (!reponse.isCorrect) {
                result += '\n<answer fraction="0" format="moodle_auto_format">' +
                    '\n<text>' + reponse.label + '</text>' +
                    '\n<feedback format="moodle_auto_format">' +
                    '\n<text></text>' +
                    '\n</feedback>' +
                    '\n</answer>';
            }
            else { console.log("error"); }
        });
        result += '\n</question>\n';
    }
    return result;
}
function XQouverte(question, NumNom) {
    let result = "";
    result += '\n<question type="essay">' +
        '\n<name>' +
        '\n<text>' + NumNom + '</text>' +
        '\n</name>' +
        '\n<questiontext format="moodle_auto_format">' +
        '\n<text>' + question + '</text>' +
        '\n</questiontext>' +
        '\n<generalfeedback format="moodle_auto_format">' +
        '\n<text></text>' +
        '\n</generalfeedback>' +
        '\n<defaultgrade>1.0000000</defaultgrade>' +
        '\n<penalty>0.3333333</penalty>' +
        '\n<hidden>0</hidden>' +
        '\n<idnumber></idnumber>' +
        '\n<responseformat>editor</responseformat>' +
        '\n<responserequired>1</responserequired>' +
        '\n<responsefieldlines>15</responsefieldlines>' +
        '\n<minwordlimit></minwordlimit>' +
        '\n<maxwordlimit></maxwordlimit>' +
        '\n<attachments>0</attachments>' +
        '\n<attachmentsrequired>0</attachmentsrequired>' +
        '\n<maxbytes>0</maxbytes>' +
        '\n<filetypeslist></filetypeslist>' +
        '\n<graderinfo format="html">' +
        '\n<text></text>' +
        '\n</graderinfo>' +
        '\n<responsetemplate format="html">' +
        '\n<text></text>' +
        '\n</responsetemplate>' +
        '\n</question>\n'
    return result;
}
function XQChoixMultipleP(question, NumNom, rep) {
    let result = "";
    result += '\n<question type="multichoice">' +
        '\n<name>' +
        '\n<text>' + NumNom + '</text>' +
        '\n</name>' +
        '\n<questiontext format="moodle_auto_format">' +
        '\n<text>' + question + '</text>' +
        '\n</questiontext>' +
        '\n<generalfeedback format="moodle_auto_format">' +
        '\n<text></text>' +
        '\n</generalfeedback>'
    '\n<defaultgrade>1.0000000</defaultgrade>' +
        '\n<penalty>0.3333333</penalty>' +
        '\n<hidden>0</hidden>' +
        '\n<idnumber></idnumber>' +
        '\n<single>false</single>' +
        '\n<shuffleanswers>true</shuffleanswers>' +
        '\n<answernumbering>abc</answernumbering>' +
        '\n<showstandardinstruction>0</showstandardinstruction>' + '\n<correctfeedback format="moodle_auto_format">' +
        '\n<text></text>' +
        '\n</correctfeedback>' +
        '\n<partiallycorrectfeedback format="moodle_auto_format">' +
        '\n<text></text>' +
        '\n</partiallycorrectfeedback>' +
        '\n<incorrectfeedback format="moodle_auto_format">' +
        '\n<text></text>' +
        '\n</incorrectfeedback>';
    if (rep.length < 2 || rep.length > 5) {
        console.log("tableau de réponse de mauvaise taille");
        return;
    }
    else {
        // result += "\n{\n";
        let nbCorrect = 0;

        rep.forEach(reponse => {
            if (reponse.isCorrect) nbCorrect++;
        });

        nbCorrect = 100 / nbCorrect;
        nbCorrect = Math.round(nbCorrect * 100000) / 100000;
        rep.forEach(reponse => {
            // result+=reponse.string+" ";
            if (reponse.isCorrect) {
                result += '\n<answer fraction="' + nbCorrect + '" format="moodle_auto_format">' +
                    '\n<text>' + reponse.label + '</text>' +
                    '\n<feedback format="moodle_auto_format">' +
                    '\n<text></text>' +
                    '\n</feedback>' +
                    '\n</answer>';
            }
            else if (!reponse.isCorrect) {
                result += '\n<answer fraction="-50" format="moodle_auto_format">' +
                    '\n<text>' + reponse.label + '</text>' +
                    '\n<feedback format="moodle_auto_format">' +
                    '\n<text></text>' +
                    '\n</feedback>' +
                    '\n</answer>';

            }
            else { console.log("error"); }

        });
        result += '\n</question>\n';
    }
    return result;

}
function XQcourteString(question, NumNom, rep) {
    let result = "";
    result += '\n<question type="shortanswer">' +
        '\n<name>' +
        '\n<text>' + NumNom + '</text>' +
        '\n</name>' +
        '\n<questiontext format="moodle_auto_format">' +
        '\n<text>' + question + '</text>' +
        '\n</questiontext>' +
        '\n<generalfeedback format="moodle_auto_format">' +
        '\n<text></text>' +
        '\n</generalfeedback>' +
        '\n<defaultgrade>1.0000000</defaultgrade>' +
        '\n<penalty>0.3333333</penalty>' +
        '\n<hidden>0</hidden>' +
        '\n<idnumber></idnumber>' +
        '\n<usecase>0</usecase>';
    rep.forEach(reponse => {
        result += '\n<answer fraction="100" format="moodle_auto_format">' +
            '\n<text>' + reponse.label + '</text>' +
            '\n<feedback format="moodle_auto_format">' +
            '\n<text></text>' +
            '\n</feedback>' +
            '\n</answer>';
    });
    result += '\n</question>\n';
    return result;
}
function XQcourteNombre(question, NumNom, rep) {
    let result = "";
    if ((typeof rep) != "object") {
        console.error("reponse n'est pas un tableaux");
        return;
    }
    else if (rep.length != 2) {
        console.error("le tableau reponse contient le mauvais nombre de réponse");
        return;
    }
    else if (isNaN(rep[0].label)) {
        console.error("la réponse n'est pas un nombre");
        return;
    }
    else if (rep[0].isCorrect == rep[1].isCorrect) {
        console.error("les 2 reponses ont le meme bool impossible de différencier la marge de la valeur");
        return;
    }
    else {
        if (rep[0].isCorrect == true) {
            result += '\n<question type="numerical">' +
                '\n<name>' +
                '\n<text>' + NumNom + '</text>' +
                '\n</name>' +
                '\n<questiontext format="moodle_auto_format">' +
                '\n<text>' + question + '</text>' +
                '\n</questiontext>' +
                '\n<generalfeedback format="moodle_auto_format">' +
                '\n<text></text>' +
                '\n</generalfeedback>' +
                '\n<defaultgrade>1.0000000</defaultgrade>' +
                '\n<penalty>0.3333333</penalty>' +
                '\n<hidden>0</hidden>' +
                '\n<idnumber></idnumber>' +
                '\n<answer fraction="100" format="moodle_auto_format">' +
                '\n<text>' + rep[0].label + '</text>' +
                '\n<feedback format="moodle_auto_format">' +
                '\n<text></text>' +
                '\n</feedback>' +
                '\n<tolerance>0</tolerance>' +
                '\n</answer>' +
                '\n<answer fraction="50" format="moodle_auto_format">' +
                '\n<text>' + rep[0].label + '</text>' +
                '\n<feedback format="moodle_auto_format">' +
                '\n<text></text>' +
                '\n</feedback>' +
                '\n<tolerance>' + rep[1].label + '</tolerance>' +
                '\n</answer>' +
                '\n<unitgradingtype>0</unitgradingtype>' +
                '\n<unitpenalty>1.0000000</unitpenalty>' +
                '\n<showunits>3</showunits>' +
                '\n<unitsleft>0</unitsleft>' +
                '\n</question>\n';
            return result;
        }
        else {
            result += '\n<question type="numerical">' +
                '\n<name>' +
                '\n<text>' + NumNom + '</text>' +
                '\n</name>' +
                '\n<questiontext format="moodle_auto_format">' +
                '\n<text>' + question + '</text>' +
                '\n</questiontext>' +
                '\n<generalfeedback format="moodle_auto_format">' +
                '\n<text></text>' +
                '\n</generalfeedback>' +
                '\n<defaultgrade>1.0000000</defaultgrade>' +
                '\n<penalty>0.3333333</penalty>' +
                '\n<hidden>0</hidden>' +
                '\n<idnumber></idnumber>' +
                '\n<answer fraction="100" format="moodle_auto_format">' +
                '\n<text>' + rep[1].label + '</text>' +
                '\n<feedback format="moodle_auto_format">' +
                '\n<text></text>' +
                '\n</feedback>' +
                '\n<tolerance>0</tolerance>' +
                '\n</answer>' +
                '\n<answer fraction="50" format="moodle_auto_format">' +
                '\n<text>' + rep[1].label + '</text>' +
                '\n<feedback format="moodle_auto_format">' +
                '\n<text></text>' +
                '\n</feedback>' +
                '\n<tolerance>' + rep[0].label + '</tolerance>' +
                '\n</answer>' +
                '\n<unitgradingtype>0</unitgradingtype>' +
                '\n<unitpenalty>1.0000000</unitpenalty>' +
                '\n<showunits>3</showunits>' +
                '\n<unitsleft>0</unitsleft>' +
                '\n</question>\n';
            return result;
        }
    }
}

function questionX(questionObject, NumNom) {
    let result = "";
    switch (questionObject.type) {
        case 1:
            result = XQChoixMultiple(questionObject.label, NumNom, questionObject.answers);

            break;
        case 5:
            result = XQouverte(questionObject.label, NumNom);

            break;
        case 2:
            result = XQChoixMultipleP(questionObject.label, NumNom, questionObject.answers);

            break;
        case 4:
            result = XQcourteString(questionObject.label, NumNom, questionObject.answers);

            break;
        case 3:
            result = XQcourteNombre(questionObject.label, NumNom, questionObject.answers);
            break;

        default:
            break;
    }
    return result;
}
function quizzXml(quizz) {
    let result= '';
     result = '<?xml version="1.0" encoding="UTF-8"?>\n<quiz>';

    quizz.questions.forEach(quest => {
        if (quest.subject == undefined || quest.subject == null) {
            result += questionX(quest, quizz.label + (quizz.questions.indexOf(quest) + 1).toString()).toString()  ;

        }
        else {
            result += 
                '\n<question type="category">' +
                '\n<category>' +
                '\n<text>' +quest.subject+ '</text>' +
                '\n</category>' +
                '\n</question>\n' + questionX(quest, quizz.label + (quizz.questions.indexOf(quest) + 1).toString()).toString()  ;
        }
    });
    return result;
}


// let reponse = {
//     label: "test",
//     isCorrect: false
// };
// let reponse2 = {
//     label: "101",
//     isCorrect: true
// };
// let reponse3 = {
//     label: "2",
//     isCorrect: false
// };
// let reponse4 = {
//     label: "qwerty3",
//     isCorrect: true
// };
// let reponse5 = {
//     label: "nstjs",
//     isCorrect: false
// };

// let tabRep1 = [reponse, reponse2, reponse3];
// let tabRep2 = [reponse, reponse2, reponse3, reponse4, reponse5];
// let tabRep3 = [reponse, reponse2];
// let tabRep4 = [reponse2, reponse3];

// let question1 = {
//     label: "c'est la premiere question ?",
//     type: 1,
//     subject: "javascript",
//     answers: tabRep1
// }
// let question2 = {
//     label: "c'est la 2 question ?",
//     type: 5,
//     subject: "BDD",
//     answers: tabRep2
// }
// let question3 = {
//     label: "c'est la 3 question ?",
//     type: 2,
//     subject: "PHP",
//     answers: tabRep2
// }
// let question4 = {
//     label: "c'est la 4 question ?",
//     type: 4,
//     subject: "BDD",
//     answers: tabRep3
// }
// let question5 = {
//     label: "c'est la 5 question ?",
//     type: 3,
//     answers: tabRep4
// }

// let tabQuestion = [question1, question2, question3, question4, question5];

// let quizzT = {
//     label: "quizzTest",
//     questions: tabQuestion
// }

// let quizzJson = {
//     "label": "Test",
//     "subject": "JavaScript",
//     "subject-id": 3,
//     "questions": [
//         {
//             "label": "rere",
//             "subject": "JavaScript",
//             "subject-id": 3,
//             "id": 1,
//             "type": 3,
//             "answers": [
//                 {
//                     "label": "14",
//                     "id": 1,
//                     "isCorrect": true
//                 },
//                 {
//                     "label": "0.5",
//                     "id": 2,
//                     "isCorrect": false
//                 }
//             ]
//         },
//         {
//             "label": "teeeeeeeestettetet",
//             "subject": "Maths",
//             "subject-id": 1,
//             "id": 2,
//             "type": 4,
//             "answers": [
//                 {
//                     "label": "rererererrrggg",
//                     "id": 3,
//                     "isCorrect": true
//                 },
//                 {
//                     "label": "rererererttgfgfgfg",
//                     "id": 4,
//                     "isCorrect": true
//                 }
//             ]
//         },
//         {
//             "label": "test test",
//             "subject": "Géo",
//             "subject-id": 2,
//             "id": 3,
//             "type": 1,
//             "answers": [
//                 {
//                     "label": "etete",
//                     "id": 5,
//                     "isCorrect": false
//                 },
//                 {
//                     "label": "ererere",
//                     "id": 6,
//                     "isCorrect": true
//                 }
//             ]
//         }
//     ]
// }

//console.log(quizzGift(quizzJson));
// console.log(quizzXml(quizzJson));

//console.log(quizzGift(quizzT));
//console.log(quizzXml(quizzT));

// console.log(GQChoixMultiple("testCMR", 1, tabRep));
// console.log(GQouverte("abracadabra ?", 2));
// console.log(GQChoixMultipleP("testCMC", 3, tabRep2));
// console.log(GQcourteString("courte", 4, tabRep3));
// console.log(GQcourteNombre("fluf", 5, 3, 1516));
// console.log(GQcourteNombre("testcourtenombre", 6, 0.5, tabRep4))

function download(filename, textInput) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8, ' + encodeURIComponent(textInput));
    element.setAttribute('download', filename);
    document.body.appendChild(element);
    element.click();
    //document.body.removeChild(element);
}


let _dataHolder;
let _quizId;
document.addEventListener("DOMContentLoaded", () => {

    _dataHolder = document.getElementById("data-holder");
  
    _quizId = _dataHolder.dataset.quizId;

    document.getElementById("btnGift")
        .addEventListener("click", function () {

            getQuizJSONForDL("/quizs/"+_quizId+"/fetch") .then((data) => {
                console.log(data);
          
                
                download(data.label + ".GIFT", quizzGift(data));
          
              })
              .catch((err) => {
                console.log('error in "addEventListener" : ' + err);
              });

            // var text = document.getElementById("text").value;
            // var filename = "output.GIFT";
        }, false);
})

document.addEventListener("DOMContentLoaded", () => {
    document.getElementById("btnXml")
        .addEventListener("click", function () {
            // var text = document.getElementById("text").value;
            // var filename = "output.GIFT";

            getQuizJSONForDL("/quizs/"+_quizId+"/fetch") .then((data) => {
                console.log(data);
          
                
                download(data.label + ".xml", quizzXml(data));
          
              })
              .catch((err) => {
                console.log('error in "addEventListener" : ' + err);
              });


           
        }, false);
})



async function getQuizJSONForDL(url) {
    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-type": "application/json",
      },
    });
    const responseData = await response.json();
    return responseData;
  }