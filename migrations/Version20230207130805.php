<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230207130805 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__quiz_question AS SELECT id, related_quiz_id, question_label FROM quiz_question');
        $this->addSql('DROP TABLE quiz_question');
        $this->addSql('CREATE TABLE quiz_question (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, related_quiz_id INTEGER NOT NULL, question_type_id INTEGER NOT NULL, subject_id INTEGER NOT NULL, question_label VARCHAR(255) NOT NULL, CONSTRAINT FK_6033B00BBA251BD0 FOREIGN KEY (related_quiz_id) REFERENCES quiz (id) ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_6033B00BCB90598E FOREIGN KEY (question_type_id) REFERENCES question_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_6033B00B23EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO quiz_question (id, related_quiz_id, question_label) SELECT id, related_quiz_id, question_label FROM __temp__quiz_question');
        $this->addSql('DROP TABLE __temp__quiz_question');
        $this->addSql('CREATE INDEX IDX_6033B00BBA251BD0 ON quiz_question (related_quiz_id)');
        $this->addSql('CREATE INDEX IDX_6033B00BCB90598E ON quiz_question (question_type_id)');
        $this->addSql('CREATE INDEX IDX_6033B00B23EDC87 ON quiz_question (subject_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__quiz_question AS SELECT id, related_quiz_id, question_label FROM quiz_question');
        $this->addSql('DROP TABLE quiz_question');
        $this->addSql('CREATE TABLE quiz_question (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, related_quiz_id INTEGER NOT NULL, question_label VARCHAR(255) NOT NULL, CONSTRAINT FK_6033B00BBA251BD0 FOREIGN KEY (related_quiz_id) REFERENCES quiz (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO quiz_question (id, related_quiz_id, question_label) SELECT id, related_quiz_id, question_label FROM __temp__quiz_question');
        $this->addSql('DROP TABLE __temp__quiz_question');
        $this->addSql('CREATE INDEX IDX_6033B00BBA251BD0 ON quiz_question (related_quiz_id)');
    }
}
