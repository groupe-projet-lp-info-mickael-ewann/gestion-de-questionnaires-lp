<?php

namespace App\Command;

use App\Entity\Category;
use App\Entity\PremadeAnswer;
use App\Entity\QuizQuestion;
use App\Repository\CategoryRepository;
use App\Repository\QuestionRepository;
use App\Repository\QuizQuestionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\MakerBundle\Validator;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[AsCommand(
    name: 'app:create-question',
    description: 'Create a question',
)]
class CreateQuestionCommand extends Command
{


    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em, 
    ValidatorInterface $vi,
    QuizQuestionRepository $questionRepo){
        
        $this->em = $em;
        $this->vi = $vi;
        $this->questionRepo = $questionRepo;
        parent::__construct();
    }



    private QuizQuestionRepository $questionRepo;

    private ValidatorInterface $vi;



    protected function configure(): void
    {
        $this
            // ->addArgument('name', InputArgument::REQUIRED, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);



        $questions = $this->questionRepo->findAll();

        if(count($questions) > 0){
            $io->text("All questions in database:");
            foreach ($questions as $key => $value) {
                $io->text($value->getQuestionLabel());
            }
        }else{
            $io->text("No question in database.");
        }
       
        $confirmCreate =   $io->confirm('Do you want to create a new question?', true);

        if($confirmCreate){

        }else{
            return Command::SUCCESS;
        }


            $labelToUse =  $io->ask('Give a label for the new Question:');
    
        $allAnswers = array();

        $userFinishedAddingPremadeAnswers = false;

        while($userFinishedAddingPremadeAnswers == false){

           
                $answerLabel =  $io->ask('Give a label for the new PremadeAnswer:');
            

          
                $isCorrect =  $io->confirm('Is the answer supposed to be correct?', false);
           
            $answerToAdd = new PremadeAnswer();
            $answerToAdd->setAnswerLabel($answerLabel);
            $answerToAdd->setIsCorrect($isCorrect);

            array_push($allAnswers, $answerToAdd);
            $atLeastOneRightAnswer = false;
            $atLeastOneWrongAnswer = false;
            

            if(count($allAnswers)>1){
                foreach ($allAnswers as $key => $value) {
                    $io->text($value->getIsCorrect());

                    if($value->getIsCorrect()){
                        $atLeastOneRightAnswer = true;
                    }else{
                        $atLeastOneWrongAnswer = true;
                    }

                }

                if($atLeastOneRightAnswer && $atLeastOneWrongAnswer){
                    $userFinishedAddingPremadeAnswers =  !$io->confirm('Do you want to add another answer?', false);
                }
            }
        }


        $question = (new QuizQuestion)
        ->setQuestionLabel($labelToUse);

        $this->em->persist($question);

        foreach ($allAnswers as $key => $value) {
            $value->setRelatedQuestion($question);
            $this->em->persist($value);
        }

       


        // $category = (new Category)->setCategoryLabel($labelToUse);
        // $this->em->persist($category);
        $this->em->flush();

     //   dd($category);



         $questions = $this->questionRepo->findAll();

        $io->text("All questions in database:");
        foreach ($questions as $key => $value) {
            $io->text($value->getQuestionLabel());
        }

     
       
        return Command::SUCCESS;
    }
}
