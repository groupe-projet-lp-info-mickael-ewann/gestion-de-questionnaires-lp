<?php

namespace App\Entity;

use App\Repository\QuizQuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: QuizQuestionRepository::class)]
class QuizQuestion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $questionLabel = null;

    #[ORM\OneToMany(mappedBy: 'relatedQuestion', targetEntity: PremadeAnswer::class, orphanRemoval: true)]
    private Collection $premadeAnswers;

    #[ORM\ManyToOne(inversedBy: 'quizQuestions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Quiz $relatedQuiz = null;

    #[ORM\ManyToOne(inversedBy: 'quizQuestions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?QuestionType $questionType = null;

    #[ORM\ManyToOne(inversedBy: 'quizQuestions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Subject $subject = null;

    public function __construct()
    {
        $this->premadeAnswers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestionLabel(): ?string
    {
        return $this->questionLabel;
    }

    public function setQuestionLabel(string $questionLabel): self
    {
        $this->questionLabel = $questionLabel;

        return $this;
    }

    /**
     * @return Collection<int, PremadeAnswer>
     */
    public function getPremadeAnswers(): Collection
    {
        return $this->premadeAnswers;
    }

    public function addPremadeAnswer(PremadeAnswer $premadeAnswer): self
    {
        if (!$this->premadeAnswers->contains($premadeAnswer)) {
            $this->premadeAnswers->add($premadeAnswer);
            $premadeAnswer->setRelatedQuestion($this);
        }

        return $this;
    }

    public function removePremadeAnswer(PremadeAnswer $premadeAnswer): self
    {
        if ($this->premadeAnswers->removeElement($premadeAnswer)) {
            // set the owning side to null (unless already changed)
            if ($premadeAnswer->getRelatedQuestion() === $this) {
                $premadeAnswer->setRelatedQuestion(null);
            }
        }

        return $this;
    }

    public function getRelatedQuiz(): ?Quiz
    {
        return $this->relatedQuiz;
    }

    public function setRelatedQuiz(?Quiz $relatedQuiz): self
    {
        $this->relatedQuiz = $relatedQuiz;

        return $this;
    }

    public function getQuestionType(): ?QuestionType
    {
        return $this->questionType;
    }

    public function setQuestionType(?QuestionType $questionType): self
    {
        $this->questionType = $questionType;

        return $this;
    }

    public function getSubject(): ?Subject
    {
        return $this->subject;
    }

    public function setSubject(?Subject $subject): self
    {
        $this->subject = $subject;

        return $this;
    }
}
