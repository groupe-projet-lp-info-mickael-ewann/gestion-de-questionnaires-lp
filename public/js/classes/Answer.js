class Answer {

    constructor(id, label, isCorrect){
        this._id = id;
        this._label = label;
        console.log("ADDED LABEL "+this._label)
        this._isCorrect = isCorrect;
    }

    get id(){
        return this._id;
    }

    get isCorrect(){
        return this._isCorrect;
    }

    get label(){
        return this._label;
    }

    overrideLabel(label){
        this._label = label;
    }
    
    overrideIsCorrect(isCorrect){
        this._isCorrect = isCorrect;
    }

}